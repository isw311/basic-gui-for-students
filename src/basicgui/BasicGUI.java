/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui;

import javax.swing.JOptionPane;
import basicgui.database.DataAccess;
import basicgui.database.DataAccessMySQL;
import basicgui.database.Result;
import basicgui.views.Login;


/**
 *
 * @author mismatso
 */
public class BasicGUI {
    
    static DataAccess dataAccess;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        
        // Se inicializa el objeto de conexión a BD
        dataAccess = new DataAccessMySQL(
            "localhost", 
            3306, 
            "isw311_simpleLogin",
            "usr_isw311", 
            "12345");
        
        // Se realiza la conexión y se captura el resultado
        Result result = dataAccess.connect();
        
        // Se verifica si ocurrio algún error con la conexión
        if (result.isError())
        {
            JOptionPane.showMessageDialog(
                null, result.getErrorDescription());
        }
        else
        {
            // Se crea y se visualiza el formulario
            java.awt.EventQueue.invokeLater(() -> {
                //new Login(dataAccess).setVisible(true);
                
                Login login = new Login(dataAccess);
                login.setLocationRelativeTo(null);
                login.setVisible(true);
                
                
            });
        }
    }
    
}
