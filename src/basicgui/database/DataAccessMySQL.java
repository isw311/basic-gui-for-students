/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.database;

/**
 *
 * @author mismatso
 */
public class DataAccessMySQL extends DataAccess {

    /*
        Datos genéricos
        --------------------------------
        String server, 
        int port, 
        String database,
        String schema, 
        String username, 
        String password, 
        TypeOfDatabase typeOfDatabase
    */
    
    public DataAccessMySQL(
            String server, 
            int port, 
            String database,
            String username, 
            String password) {
        super(server, port, database, "", 
                username, password, TypeOfDatabase.MySQL);
        
        // jdbc:mysql://localhost:3306/simpleLogin
        super.connectionString = TypeOfDatabase.MySQL.getConnectionString() +
                server + ":" + port + "/" + database;
    }

}
