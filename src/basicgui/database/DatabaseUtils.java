/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author mismatso
 */
public class DatabaseUtils {
    
    static public DefaultTableModel getDefaultTableModel(ResultSet rs, String[] indetifiers)
    {
        DefaultTableModel dtm = new DefaultTableModel()
        {
            @Override
            public boolean isCellEditable(int row, int col)
            {
                return false;
            }
        };

        try {
            dtm.setColumnIdentifiers(getIndetifiers(rs, indetifiers));
            while (rs.next())
            {
                Object[] rowData = new Object[indetifiers.length];
                for (int i = 0; i < rowData.length; ++i)
                {
                    rowData[i] = rs.getObject(i+1);
                }
                dtm.addRow(rowData.clone());
            }
            return dtm;
        } catch (SQLException e) {
            Logger.getLogger(DatabaseUtils.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    static public String[] getIndetifiers(ResultSet rs, String[] indetifiers)
    {
        if (indetifiers == null)
        {
            try {
                ResultSetMetaData metaData = rs.getMetaData();
                int numberOfColumns = metaData.getColumnCount();
                
                // Recuperar el nombre de las columnas de la base de datos
                for (int column = 0; column < numberOfColumns; column++) {
                    indetifiers[column] = metaData.getColumnLabel(column + 1);    
                }
                return indetifiers;
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseUtils.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        else
            return indetifiers;
    }
}
