/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mismatso
 */
public abstract class DataAccess {
    
    // Atributos genéricos para todas la base de datos
    protected final String server;
    protected final int port;
    protected final String database;
    protected final String schema;
    protected final String username;
    protected final String password;
    
    // Atributos para manejar la conexión
    protected String connectionString;
    protected Connection connection;
    protected ResultSet resultSet = null;
    protected Statement statement;
    
    protected TypeOfDatabase typeOfDatabase;

    public DataAccess(
            String server, 
            int port, 
            String database,
            String schema, 
            String username, 
            String password, 
            TypeOfDatabase typeOfDatabase) {
        
        this.server = server;
        this.port = port;
        this.database = database;
        this.schema = schema;
        this.username = username;
        this.password = password;
        this.typeOfDatabase = typeOfDatabase;
    }
    
    public Result connect() { /* Antes no retornaba nada (void) */
        Result result = new Result();
        try {
            Class.forName(typeOfDatabase.getDriver());
        } catch (ClassNotFoundException ex) {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            result.setError(ex.getLocalizedMessage());
          }
        
        try {
            connection = DriverManager.getConnection(connectionString,
                    username, password);
        } catch (SQLException ex) {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }

    public Result disconnect() {
        Result result = new Result();
        try {
            connection.close();
        } catch (SQLException ex) {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }

    public boolean isClosed() {
        try {
            return connection.isClosed();
        } catch (SQLException ex) {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public ResultSetCustomized executeSqlQuery(String sql) {
        ResultSetCustomized result = new ResultSetCustomized();
        
        try {
            statement = connection.createStatement();
            // Agregar el ResultSet a nuestra clase personalizada
            result.setResultSet(statement.executeQuery(sql));
        } catch (SQLException ex) {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }
    
    public ResultSetCustomized executeSqlQuery(PreparedStatement pstmt) {
        ResultSetCustomized result = new ResultSetCustomized();
        try {
            result.setResultSet(pstmt.executeQuery());
        } catch (SQLException e) {
            result.setError(e.getLocalizedMessage());
        }
        return result;
    }

    public Result executeSQL(PreparedStatement pstmt) {
        Result result = new Result();
        try
        {
            pstmt.executeUpdate();
            pstmt.close();
        }
        catch (SQLException ex)
        {
            //Logger.getLogger(DataAccessMySQL.class.getName()).log(Level.SEVERE, null, ex);
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }

    public String getSchema() {
        if (schema.isEmpty())
            return schema;
        else
            return "\"" + schema + "\".";
    }
    
    public Connection getConnection() {
        return connection;
    }
}
