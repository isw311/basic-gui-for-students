/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.database;

/**
 *
 * @author mismatso
 */
public class Result {
    
    // Atributos para determinar errores
    private boolean error;
    private String errorDescription;

    public Result() {
        this.error = false; // Por defecto se inicaliza sin error
    }
    
    public Result(boolean error, String errorDescription) {
        this.error = error;
        this.errorDescription = errorDescription;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
    
    public void setError(String errorDescription) {
        this.error = true;
        this.errorDescription = errorDescription;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
