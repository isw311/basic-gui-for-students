/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.database;

/**
 *
 * @author mismatso
 */
public class DataAccessPostgreSQL extends DataAccess {

    public DataAccessPostgreSQL(
            String server, 
            int port, 
            String database,
            String schema,
            String username, 
            String password) {
        super(server, port, database, schema, 
                username, password, TypeOfDatabase.PostgreSQL);
        
        // jdbc:postgresql://localhost:3306/simpleLogin
        connectionString = TypeOfDatabase.PostgreSQL.getConnectionString() + 
                server + ":" + port + "/" + database + "?searchpath=" + schema;
    }

}
