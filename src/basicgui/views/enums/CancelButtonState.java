/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.views.enums;

/**
 *
 * @author mismatso
 */
public enum CancelButtonState {
    Enable(true), Disable(false);

    private final boolean enable;

    CancelButtonState(Boolean state) {
        this.enable = state;
    }

    public boolean isEnable() {
        return enable;
    }
}
