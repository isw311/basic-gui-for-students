/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.views;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

/**
 *
 * @author mismatso
 */
public final class JDesktopPaneCustomized extends JDesktopPane {
    
    public static int LIGHT_BLUE = 0;
    public static int GREEN = 1;
    public static int BLUE = 2;
    public static int RED_BLUE = 3;
    
    private final String[] WALLPAPERS = {
        "../images/wallpapers/lightblue.jpg",
        "../images/wallpapers/green.jpg",
        "../images/wallpapers/blue.jpg",
        "../images/wallpapers/redblue.jpg"
    };
    
    private String background;
    private Integer backgroundColorIndex;

    public void setBackgroundColorIndex(int backgroundColorIndex) {
        this.background = WALLPAPERS[backgroundColorIndex];
        this.backgroundColorIndex = backgroundColorIndex;
    }
    
    public Integer getBackgroundColorIndex() {
        return backgroundColorIndex;
    }

    public JDesktopPaneCustomized() {
        super();
        this. background = getRamdomWallpaper();
    }
    
    public String getRamdomWallpaper(){
        this.backgroundColorIndex = new Random().nextInt(WALLPAPERS.length);
        return WALLPAPERS[this.backgroundColorIndex];
    }
    
    @Override
    protected void paintComponent(Graphics graphics)
    {
        Image image = new ImageIcon(this.getClass().getResource(this.background)).getImage();

        int x = this.getWidth();
        int y = this.getHeight();
        
        try {
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.drawImage(image, 0, 0, x, y, null);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
