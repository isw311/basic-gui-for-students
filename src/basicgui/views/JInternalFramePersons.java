/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import basicgui.database.DataAccess;
import basicgui.database.ResultSetCustomized;
import basicgui.database.DatabaseUtils;
import basicgui.database.Result;
import basicgui.models.Persons;

/**
 *
 * @author mismatso
 */
public class JInternalFramePersons
        extends JInternalFrameWithToolbar
{
    
    private Integer id = null; //PK del objecto seleccionado
    private final DataAccess dataAccess;
    private final Persons persons = new Persons();
    
    // Variables para el filtro
    private String nameFilter;
    private String lastnameFilter;

    /**
     * Creates new form JInternalFramePersons
     * @param dataAccess
     */
    public JInternalFramePersons(DataAccess dataAccess) {
        initComponents();
        this.dataAccess = dataAccess;
        
        crudToolBar.setToolbarListeners();
        this.setStateOfGroup(jpControls, false);
        this.refreshData(false);
        this.enableRowSelectionListener();
    }
    
    /**
     * Habilita un "listener" que se dispara cada vez que se selecciona una fila
     * en la JTable que contiene los registros
     */
    private void enableRowSelectionListener()
    {
        ListSelectionModel lsm = jtRecords.getSelectionModel();
        lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lsm.addListSelectionListener((ListSelectionEvent e) -> {
            if (jtRecords.getSelectedRow() >= 0)
                updateWithSelectedRow(jtRecords.getSelectedRow());
        });
        this.selectFirstRowIfExists();
    }
    
    /**
     * Actualiza los controles de entrada a partir de un índice de fila
     * del JTable que contiene los registros del formulario CRUD
     * 
     * @param index Índice que la fila que se cargará a los controles
     */
    private void updateWithSelectedRow(int index)
    {
        try
        {
            this.id = (Integer)jtRecords.getModel().getValueAt(index, 0);
            jtfName.setText(jtRecords.getModel().getValueAt(index, 1).toString());
            jtfLastname.setText(jtRecords.getModel().getValueAt(index, 2).toString());
        }
        catch(Exception e)
        {
            id = null;
            jtfName.setText("");
            jtfLastname.setText("");
        }
    }
    
    /**
     * Si la JTable contiene registros, selecciona la primera fila
     */
    private void selectFirstRowIfExists(){
        if (jtRecords.getRowCount() > 0)
            jtRecords.setRowSelectionInterval(0, 0);
    }
    
    /**
     * Posiciona el foco en el control de entrada principal
     */
    private void primaryControlRequestFocus()
    {
        this.jtfName.requestFocus();
    }

    /**
     * Limpia los controles de entrada
     */
    private void clearControls()
    {
        jtfName.setText("");
        jtfLastname.setText("");
    }
    
    /**
     * Establece el estado del grupo de controles de entrada y de la JTable
     * @param panelGroup JPanel que contiene los controles de entrada
     * @param state Bandera de estado ("true" o "flase")
     */
    private void setStateOfGroup(JPanel panelGroup, Boolean state)
    {
        for (Component c : panelGroup.getComponents()) {
            c.setEnabled(state);
        }
        this.jtRecords.setEnabled(!state);
    }
    
    /**
     * Utiliza la conexiónd de base de datos para recuperar los registros, si
     * la bandera de filtrado está actividad recupera solamente los registros
     * que hacen "match" con los parámetros del filtro
     * 
     * @param isFiltered Bandera de filtrado ("true" o "flase")
     */
    private void refreshData(boolean isFiltered)
    {   
        ResultSetCustomized rs;
        if (!isFiltered)
            rs = persons.getRecords(dataAccess);
        else
            rs = persons.getRecords(dataAccess, this.nameFilter, this.lastnameFilter);
            
        if (rs.isError())
        {
            // Si ocurrió algún error con la consulta a base de datos
            JOptionPane.showMessageDialog(
                    this,
                    rs.getErrorDescription(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            // Si la consulta fue exitosa se carga el Resultset al "table model"
            DefaultTableModel dataModel = 
                DatabaseUtils.getDefaultTableModel(
                        rs.getResultSet(), persons.getIndetifiers());
        
            jtRecords.setModel(dataModel);
            dataModel.fireTableDataChanged();
            
            // Remueve del JTable la columna 0, que contiene el ID del registro
            jtRecords.removeColumn(jtRecords.getColumnModel().getColumn(0));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlImage = new javax.swing.JLabel();
        jlTitle = new javax.swing.JLabel();
        jpControls = new javax.swing.JPanel();
        jlName = new javax.swing.JLabel();
        jlLastname = new javax.swing.JLabel();
        jtfName = new javax.swing.JTextField();
        jtfLastname = new javax.swing.JTextField();
        jpTable = new javax.swing.JPanel();
        jspTableScrollPanel = new javax.swing.JScrollPane();
        jtRecords = new javax.swing.JTable();
        crudToolBar = new basicgui.views.JPanelCRUDToolbar();

        setClosable(true);

        jPanel1.setBackground(new java.awt.Color(247, 247, 247));
        jPanel1.setPreferredSize(new java.awt.Dimension(450, 88));

        jlImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/basicgui/images/persons_64.png"))); // NOI18N

        jlTitle.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        jlTitle.setText("Mantenimiento de Personas");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jlImage)
                .addGap(40, 40, 40)
                .addComponent(jlTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlImage)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jlTitle)))
                .addContainerGap())
        );

        jlName.setText("Nombre");

        jlLastname.setText("Apellido");

        javax.swing.GroupLayout jpControlsLayout = new javax.swing.GroupLayout(jpControls);
        jpControls.setLayout(jpControlsLayout);
        jpControlsLayout.setHorizontalGroup(
            jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlName)
                    .addComponent(jlLastname))
                .addGap(33, 33, 33)
                .addGroup(jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtfName)
                    .addComponent(jtfLastname, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpControlsLayout.setVerticalGroup(
            jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlName)
                    .addComponent(jtfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlLastname)
                    .addComponent(jtfLastname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jtRecords.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jspTableScrollPanel.setViewportView(jtRecords);

        javax.swing.GroupLayout jpTableLayout = new javax.swing.GroupLayout(jpTable);
        jpTable.setLayout(jpTableLayout);
        jpTableLayout.setHorizontalGroup(
            jpTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jspTableScrollPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE)
        );
        jpTableLayout.setVerticalGroup(
            jpTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jspTableScrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpControls, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(crudToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(crudToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Variables declared from the interface designer">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private basicgui.views.JPanelCRUDToolbar crudToolBar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlImage;
    private javax.swing.JLabel jlLastname;
    private javax.swing.JLabel jlName;
    private javax.swing.JLabel jlTitle;
    private javax.swing.JPanel jpControls;
    private javax.swing.JPanel jpTable;
    private javax.swing.JScrollPane jspTableScrollPanel;
    private javax.swing.JTable jtRecords;
    private javax.swing.JTextField jtfLastname;
    private javax.swing.JTextField jtfName;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>
    
    // <editor-fold defaultstate="expanded" desc="Button event handlers">
    @Override
    public boolean jbNewActionPerformed(ActionEvent evt) {
        this.clearControls();
        this.setStateOfGroup(jpControls, true);
        this.jtfName.requestFocus();
        return true;
    }

    @Override
    public boolean jbEditActionPerformed(ActionEvent evt) {
        if (jtRecords.getSelectedRow() >= 0)
        {
            this.setStateOfGroup(jpControls, true);
            this.primaryControlRequestFocus();
            return true;
        }
        else
        {
            JOptionPane.showMessageDialog(this,
                    "Debe seleccionar el registro que desea editar",
                    "Información", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean jbSaveActionPerformed(ActionEvent evt) {
        // Validar que se hayan ingresado datos en los controles de entrada
        if (jtfName.getText().equals("") || jtfLastname.getText().equals(""))
        {
            JOptionPane.showMessageDialog(this, 
                    "Todos los datos son requeridos", "Error",
                    JOptionPane.ERROR_MESSAGE);
            
            this.primaryControlRequestFocus();
            return false;
        }
        
        // Realizar la acción que corresponda según el estado
        Result result;
        if (crudToolBar.isInserting())
        {
            result =
                persons.createRecord(
                    dataAccess, 
                    jtfName.getText(), 
                    jtfLastname.getText());
        }
        else
        {
            result =
                persons.updateRecord(
                    dataAccess, this.id, 
                    jtfName.getText(), jtfLastname.getText());
        }
        
        if (result.isError())
        {
            JOptionPane.showMessageDialog(
                    this, result.getErrorDescription(),
                    " Error", JOptionPane.ERROR_MESSAGE);
        }
        this.clearControls();
        this.setStateOfGroup(jpControls, false);
        this.crudToolBar.evalIfDisableFilter();
        this.refreshData(this.crudToolBar.isFiltered());
        this.selectFirstRowIfExists();
        return true;
    }

    @Override
    public boolean jbCancelActionPerformed(ActionEvent evt) {
        this.clearControls();
        this.setStateOfGroup(jpControls, false);
        this.updateWithSelectedRow(jtRecords.getSelectedRow());
        return true;
    }

    @Override
    public boolean jbDeleteActionPerformed(ActionEvent evt) {
        if (jtRecords.getSelectedRow() >= 0)
        {
            if (JOptionPane.showConfirmDialog(
                    this,"¿Está seguro de eliminar el registro?","Confirmación",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
                return false;
            
            // Si en el paso anterior confirmó el borrado
            Result result = persons.deleteRecord(this.dataAccess, this.id);
            if (result.isError())
            {
                JOptionPane.showMessageDialog(
                        this, result.getErrorDescription(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            this.clearControls();
            this.refreshData(crudToolBar.isFiltered());
            this.selectFirstRowIfExists();
            return true;
        }
        else
        {
            JOptionPane.showMessageDialog(this,
                    "Debe seleccionar el registro que desea eliminar",
                    "Información", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean jbFindActionPerformed(ActionEvent evt) {
        this.clearControls();
        this.setStateOfGroup(jpControls, true);
        this.primaryControlRequestFocus();
        return true;
    }

    @Override
    public boolean jbFilterActionPerformed(ActionEvent evt) {
        this.nameFilter = jtfName.getText();
        this.lastnameFilter = jtfLastname.getText();
        
        this.refreshData(true);
        this.clearControls();
        this.setStateOfGroup(jpControls, false);
        this.selectFirstRowIfExists();
        return true;
    }

    @Override
    public boolean jbReloadActionPerformed(ActionEvent evt) {
        this.nameFilter = "";
        this.lastnameFilter = "";
        
        this.refreshData(false);
        this.selectFirstRowIfExists();
        return true;
    }

    @Override
    public boolean jbExitActionPerformed(ActionEvent evt) {
        return true;
    }
    // </editor-fold>
}
