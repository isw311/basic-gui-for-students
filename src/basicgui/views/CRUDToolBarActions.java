/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.views;

import java.awt.event.ActionEvent;

/**
 *
 * @author mismatso
 */
public interface CRUDToolBarActions {
    
    public boolean jbNewActionPerformed(ActionEvent evt);
    
    public boolean jbEditActionPerformed(ActionEvent evt);

    public boolean jbSaveActionPerformed(ActionEvent evt);

    public boolean jbCancelActionPerformed(ActionEvent evt);

    public boolean jbDeleteActionPerformed(ActionEvent evt);
    
    public boolean jbFindActionPerformed(ActionEvent evt);

    public boolean jbFilterActionPerformed(ActionEvent evt);

    public boolean jbReloadActionPerformed(ActionEvent evt);
        
    public boolean jbExitActionPerformed(ActionEvent evt);
    
}
