/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgui.models;

import basicgui.database.ResultSetCustomized;
import basicgui.database.DataAccess;
import basicgui.database.Result;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author mismatso
 */
public class Persons {
    
    private DataAccess da;

    //Método que recupera todos los registros de la tabla "persons"
    public ResultSetCustomized getRecords(DataAccess da)
    {
        String sql = "SELECT id, name, last_name FROM " + da.getSchema() + "persons;";
        return da.executeSqlQuery(sql);
    }
    
    //Método que recupera todos los registros de la tabla "persons"
    public ResultSetCustomized getRecords(DataAccess da, String name, String lastname)
    {
        ResultSetCustomized result;
        PreparedStatement stmt;
        
        // Si las variable vienen en blanco se asigna un valor por defecto
        name = name.equals("") ? "%" : name;
        lastname = lastname.equals("") ? "%" : lastname;

        String sql = "SELECT id, name, last_name FROM " + da.getSchema() 
                + "persons " 
                + " WHERE name LIKE ? AND last_name LIKE ?;";
            
        try {
            stmt = da.getConnection().prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setString(2, lastname);
            result = da.executeSqlQuery(stmt);
        } catch (SQLException ex) {
            result = new ResultSetCustomized();
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }
    
    public String[] getIndetifiers(){
        String[] indetifiers = {"Id", "Nombre","Apellidos"};
        return indetifiers;
    }
    
    //Método que permite crear un nuevo registro en la tabla "persons"
    public Result createRecord(DataAccess da, String name, String lastname)
    {
        Result result;
        String sql = "INSERT INTO " + da.getSchema() + "persons(" +
                     "            name, last_name)" +
                     "    VALUES (?, ?);";
        try {
            PreparedStatement stmt = da.getConnection().prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setString(2, lastname);
            result = da.executeSQL(stmt);
        } catch (SQLException ex) {
            result = new Result();
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }
    
    //Método que permite actualizar un registro en la tabla "persons"
    public Result updateRecord(DataAccess da, Integer id, String name, String lastname)
    {
        Result result;
        String sql = "UPDATE " + da.getSchema() + "persons" +
                     "   SET  name = ?, last_name = ?" +
                     " WHERE id = ?;";
        try {
            PreparedStatement stmt = da.getConnection().prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setString(2, lastname);
            stmt.setInt(3, id);
            result = da.executeSQL(stmt);
        } catch (SQLException ex) {
            result = new Result();
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }
    
    //Método que permite eliminar un registro de la tabla "persons"
    public Result deleteRecord(DataAccess da, Integer id)
    {
        Result result;
        String sql = "DELETE FROM " + da.getSchema() + "persons" +
                     " WHERE id = ?;";
        try {
            PreparedStatement stmt = da.getConnection().prepareStatement(sql);
            stmt.setInt(1, id);
            result = da.executeSQL(stmt);
        } catch (SQLException ex) {
            result = new Result();
            result.setError(ex.getLocalizedMessage());
        }
        return result;
    }
    
}
